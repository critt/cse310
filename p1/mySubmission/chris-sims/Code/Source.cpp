#include <iostream>
#include <fstream>
#include <stdlib.h> //atoi
#include <ctime>
#include <math.h>
#include <algorithm>

using namespace std;

//instance variables
string filename; //always is instantiated with the current working name
int* pointer;	//points to array created by buildArr, and is the only array this program uses.
int mergeCounter = 0;
int mergeSwaps = 0;


//forward declarations
int getSize(string filename);
void insertionSort(string filename);
int* buildArr(string filename);
int selectK(string filename, int arr[], int index);
int selectMin(string filename, int arr[]);
void getAverage(string filename, int arr[]);
void merge(int* A, int p, int q, int r);
void mergeSort(int* arr, int p, int r);
void parallelMergeSort(int* arr, int p, int r);

//mergeSort using merge sort algorithm to sort an array
void mergeSort(int* arr, int p, int r)
{
  mergeCounter++;
  if(p < r)
  {
    int q = floor((p + r)/2);
    mergeSort(arr, p, q);
    mergeSort(arr, q+1, r);
    merge(arr, p, q, r);
  }
}

//merge is used by mergeSort and parallelMergeSort
void merge(int* A, int p, int q, int r)
{
  
  int B[r+1];
  for(int h = 0; h <= r; h++)
  {
    B[h] = A[h];
  }
  int i = p;
  int j = q+1;
  int z = p;
  mergeCounter++;
  mergeCounter++;
  while(i <= q && j <= r)
  {
    mergeCounter++;
    if(B[i] <= B[j])
    {
      A[z] = B[i];
      i++;
      mergeSwaps++;
    }
    else
    {
      A[z] = B[j];
      j++;
      mergeSwaps++;
    }
    z++;
  }

  mergeCounter++;
  mergeCounter++;
  if(i < q)
  {
    //copy(B + i, B + q + 1, A + z);
    int x = z;
    for(int m = i; m <= q; m++)
    {
      
      A[x] = B[m];
      x++;
      mergeSwaps++;
    }
  }
  else if(j < r)
  {
    //copy(B + j, B + r + 1, A + z);
    int y = z;
    for(int m = j; m <= r; m++)
    {
  
      A[y] = B[m];
      y++;
      mergeSwaps++;

    }
  }

}

//parallelMergeSort sorts the array by using a merge sort algorith in parallel
void parallelMergeSort(int* arr, int p, int r)
{
  mergeCounter++;
  if(p < r)
  {
    int q = floor((p + r)/2);
    #pragma omp parallel num_threads(2)
    {
      #pragma omp sections
      {
        #pragma omp section
          parallelMergeSort(arr, p, q);
        #pragma omp section
          parallelMergeSort(arr, q+1, r);
      }
    }
    merge(arr, p, q, r);
  }
}


//printArr prints an array
void printArr(int* arr)
{
  for( int i = 0; i <= getSize(filename) - 1; ++i)
  {
    
    cout << arr[i];
    cout << endl;

  }
}

//buildArr is always called with the start command and is always prior to InsertionSort.
//It instantates the array the program works with and returns a pointer to it.
int* buildArr(string filename)
{
	int i = 0; //counter
  	int *Q = new int[getSize(filename)]();
  	int p; //intermediary string variable
  	ifstream infile2(filename.c_str());
  	while(infile2 >> p)
  	{
  		if(i > 0)
  		{
  			Q[i-1] = p;
  		}
  		++i;
  	}
 
  	return Q;
  	infile2.close();
}

//getAverage finds the average steps and reports this information along with performance data
void getAverage(string filename, int arr[])
{
	int total = 0;
  	double avg;
    double duration;
    std::clock_t start;
    start = std::clock();
  	for (int i = getSize(filename) - 1; i >= 0; --i) 
    {
    	total = total + arr[i];
    }
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    avg = (double)total / (double)getSize(filename);

    cout << "Average number of steps: " << avg << endl;
    cout << "Time to run the average (ms): " << duration << endl;
  
}

//parallelGetAverage computes the average number of steps in parallel
void parallelGetAverage(string filename, int arr[])
{
  int total = 0;
  int firstSize = floor(getSize(filename)/2);
  int secondSize = getSize(filename) - firstSize;
    double avg;
    double duration;
    std::clock_t start;
    start = std::clock();
    #pragma omp parallel num_threads(2)
    {
      #pragma omp sections
      {
        #pragma omp section
            for (int i = 0; i < firstSize; i++) 
            {
              total = total + arr[i];
            }
        #pragma omp section
            for (int i = firstSize; i < getSize(filename); i++)
            {
              total = total + arr[i];
            }

      }
    }
   
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    avg = (double)total / (double)getSize(filename);

    cout << "Average number of steps (parallel sum): " << avg << endl;
    cout << "Time to run the average (parallel sum): " << duration << endl;
  
}

//InsertionSort performs an insertion sort algorithm on the array created by buildArr and re
//ports performance data
void insertionSort(string filename)
{
  int size = getSize(filename);
  

  int i, k, holder;
  int swaps = 0;
  int comparisons = 0;

  double duration;
  std::clock_t start;
  start = std::clock();
  for(i = 1; i < size; ++i)
  {
    ++comparisons;
    k = i;
    while(k > 0 && pointer[k-1] > pointer[k]){
      holder = pointer[k];
      pointer[k] = pointer[k-1];
      pointer[k-1] = holder;
      --k;
      ++swaps;
      ++comparisons;
    }
  }
  duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC; 

  cout << "Number of comparisons made by insertion sort: " << comparisons << endl;
  cout << "Number of swaps made by insertion sort: " << swaps << endl;
  cout << "Time to run insertion sort (ms): " << duration << endl;
  
}

//selector function for an individual index. Will return a negative int to be handled in the event of errors.
int selectK(string filename, int arr[], int index)
{
  int i;
  if(index > getSize(filename) - 1 || index == 1)
    {
      return -1;
    }
  for(i = 0; i < getSize(filename) - 1; ++i)
  {
    
    if(arr[i] > arr[i+1])
    {

      return -2; 
    }
  }
  double duration;
  std::clock_t start;
  start = std::clock();
  
  duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC; 

  cout << "Time to run the selection (ms): " << duration << endl;
  return arr[index];


}
//selector function for first index.  Will return a negative int to be handled in the event of errors.
int selectMin(string filename, int arr[])
{
  int i;
  for(i = 0; i < getSize(filename) - 1; ++i)
  {
    if(arr[i] > arr[i+1])
    {
      return 0; 
    }
  }

  double duration;
  std::clock_t start;
  start = std::clock();
  cout << "Selecting item: " << arr[0] << endl;
  duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC; 
  cout << "Time to run the selection (ms): " << duration << endl;
  return 0;

}

//selector function for last index.  Will return a negative int to be handled in the event of errors.
int selectMax(string filename, int arr[])
{
  int i;
  for(i = 0; i < getSize(filename) - 1; ++i)
  {
    if(arr[i] > arr[i+1])
    {
      return 0; 
    }
  }

  double duration;
  std::clock_t start;
  start = std::clock();
  cout << "Selecting item: " << arr[getSize(filename) - 1] << endl;
  duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC; 
  cout << "Time to run the selection (ms): " << duration << endl;
  return 0;

}

//getSize returns an int representing the size of the desired array, getting it from the first
//line of the input file
int getSize(string filename)
{
	int size;
	ifstream infile(filename.c_str());
  	if(infile.good())
  	{
    	string sLine;
    	getline(infile, sLine);
    	size = atoi(sLine.c_str());
  	}
    infile.close();
    
  	return size;
}

//main input function containing CLI interface and calls to major functions above
int input(){
	for(string line; getline(cin, line);){
		
		string delimiter = " ";
		string token1 = line.substr(0, line.find(delimiter));
		//Start
		if(token1 == "Start")
		{
			string token2 = line.substr(line.find(delimiter) + 1, line.size()-1);
			filename = token2 + ".txt";
			pointer = buildArr(filename);
			
			cout << "Processing fitness data of: " << token2 << endl;

		}
		//End (frees memory)
		else if(token1 == "End")
		{
			string token2 = line.substr(line.find(delimiter) + 1, line.size()-1);
			
			delete[] pointer;
			pointer = NULL;
			
			cout <<"End of processing fitness data for: " << token2 << endl;
		}
		//Select. This only implements selectK for the milestone, per sample input files on blackboard.
		else if(token1 == "Select")
		{
			string token2 = line.substr(line.find(delimiter) + 1, line.size()-1);
			int index = atoi(token2.c_str());
			
			int j = selectK(filename, pointer, index);
			if(j == -1)
			{
				cout << "Invalid selection." << endl;
			}
			else if(j == -2)
			{
				cout << "Unable to select from an unsorted array." << endl;
			}
			else{
				cout << "Selecting item: " << j << endl;
			}
			
		}
		//Average
		else if(token1 == "Average")
		{
			
			getAverage(filename, pointer);
		}
    //Parallel Average
    else if(token1 == "ParallelAverage")
    {
      parallelGetAverage(filename, pointer);
    }
		else if(token1 == "InsertionSort"){
			
			insertionSort(filename);
			
		}
    //Merge Sort
    else if(token1 == "MergeSort"){

      double duration;
      std::clock_t start;
      start = std::clock();

      mergeSort(pointer, 0, getSize(filename) - 1);

      duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
      cout << "Number of comparisons made by merge sort: " << mergeCounter << endl;
      cout << "Number of swaps made by merge sort: " << mergeSwaps << endl;
      cout << "Time to run merge sort (ms): " << duration << endl;
      mergeSwaps = 0;
      mergeCounter = 0;
    }
		//Parallel Merge Sort
    else if(token1 == "ParallelMergeSort")
    {
      double duration;
      std::clock_t start;
      start = std::clock();

      parallelMergeSort(pointer, 0, getSize(filename) - 1);

      duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

      
      cout << "Number of comparisons made by parallel merge sort: " << mergeCounter << endl;
      cout << "Number of swaps made by parallel merge sort: " << mergeSwaps << endl;
      cout << "Time to run parallel merge sort (ms): " << duration << endl;
      mergeSwaps = 0;
      mergeCounter = 0;
    }
    //Exit (frees memory), stops execution after.
		else if(token1 == "Exit")
		{
			delete[] pointer;
			pointer = NULL;
			cout << "Program terminating." << endl;
			return 0;
		}

	}
}

//main function
int main()
{
	input();
	return 0;
}
