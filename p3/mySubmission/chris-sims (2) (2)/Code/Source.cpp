#include <list>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stdlib.h> //atoi, atof
#include <string.h>
#include <limits>

using namespace std;

//forward variable declarations
list<int> *adj; // stores the adjacency list
vector<int> *dDist; //stores degrees of connected vertices
int *dDistArr;	//stores total occurances of degrees in the elements, correlated with the degrees themselves in the indices
int numVertex, numEdges, numCommands;	//helper variables
int maxDegreeOccurances = 0;	//helper variable
int numComponents = 0;	//counter for the number of connected components in the data set
int sizeOfComponent = 0;	//counter for the size of each component (gets reset to 0 after each component's size is determined)
bool *visited;	//array of booleans that will be used in the DFS function to keep track of which vertices have been visited



//forward function declarations
void printHistogram(int, int, int*);
void degreeDist();
void components();
void DFS(int, bool*);
void shortestPath(int, int);
void diameter();

//main input reading and command delegation function
int main()
{
	//read first line of input to find the number of vertices in the input
	cin >> numVertex;
	cin.ignore();
	//read second line of input to find the number of edges in the input
	cin >> numEdges;
	cin.ignore();
	
	//allocate the adjacency list
	adj = new list<int>[numVertex];

	//read the bulk of the input file containing the edges
	for(int i = 0; i < numEdges; i++)
	{
		int a, b;
		cin >> a >> b;

		//ignore input if it identical vertices are given
		//if not, add them to the list
		if(a != b)
		{
			adj[a].push_back(b);
            adj[b].push_back(a);
		}
		cin.ignore();
	}

	//read the first line of input following the edges to get the number of commands
	cin >> numCommands;
	cin.ignore();

	degreeDist();
	//read and process the commands
	for(int i = 0; i < numCommands; i++)
	{
		string tmp;
		cin >> tmp;
		if(!tmp.compare("degree-distribution"))
		{
			printHistogram(maxDegreeOccurances, numVertex, dDistArr);
		}
		else if(!tmp.compare("components"))
		{
			components();
		}
		else if(!tmp.compare("shortest-path"))
		{	
			int start, end;
			cin >> start >> end;
			shortestPath(start, end);
		}
		else if(!tmp.compare("diameter"))
		{
			diameter();
		}
	}

	//deallocate memory
	delete[] adj;
	adj = NULL;
	delete[] dDistArr;
	dDistArr = NULL;
	delete dDist;

}

//the primary degree-distribution function. It prepares an array to be used with a helper function that prints the histogram.
//this array is also used by the components function
void degreeDist()
{
    //this vector will contain all the degrees of every connected set sorted in descending order
    dDist = new vector<int>;

    //this array will match degrees (index) with occurances (element)
    //it is initialized to contain all 0s
    dDistArr = new int[numVertex];
    for(int i = 0; i < numVertex; i++)
    {
        dDistArr[i] = 0;
    }

	//helper variable for the following nested loop
    int degree = 1;
    //this loop iterates through the adjacency list and stores each degree in the aforementioned vector
    for(int index = 0; index < numVertex; index++)
    {
            for(list<int>::iterator it = adj[index].begin(); it != adj[index].end(); ++it)
            {
                    degree++;
            }

            //insert the degree into the vector
            dDist->push_back(degree);

            //reset helper variable for use with next adjacency list index
            degree = 1;
    }

    //sort vector in descending order
    sort(dDist->begin(), dDist->end(), std::greater<int>());
    //the array of size numVertex starts off containing 0 in all elements. 
    //iterate through the vector for each element i, find the ith element in the array, and increment it by 1.
    //in the end you will have an array in which all non zero elements contain the number of occurances of that degree
    //(that is, the array index represents the degree, and the element represents the number of occurances of that degree).
    //this also sets the max degree
    for(int i = 0; i < numVertex; i++)
    {
        dDistArr[dDist->at(i)] = dDistArr[dDist->at(i)] + 1;
        if(dDistArr[dDist->at(i)] > maxDegreeOccurances)
        {
            maxDegreeOccurances = dDistArr[dDist->at(i)];
        }
    }
}

//helper function for degreeDist that prints the histogram
void printHistogram(int maxDegreeOccurances, int numVertex, int *dDistArr)
{
    for(int i = maxDegreeOccurances; i > 0; i--)
    {
        for(int j = 0; j < numVertex; j++)
        {
            if(dDistArr[j] >= i)
            {
                cout << "*";
            }
            else cout << " ";
        }
        cout << endl;
    }
}

//this function prints the number of connected components and the number of vertices in each component
void components()
{
	//initialize an int array of size numVertex (this is an arbitrarily large yet necessarily large enough size)
	//that will store the number of vertices in each connected component
	int sizes[numVertex];

	//instantiate boolean array with size numVertex that will keep track of whether or not each vertex has already been visited by
	//the recursive depth-first-search (DFS) function. Initialize each element to false
	visited = new bool[numVertex];
	for(int i = 0; i < numVertex; i++)
	{
		visited[i] = false;
	}

	//depth first search implementation to find connected components
	for(int i = 0; i < numVertex; i++)
	{
		if(visited[i] != true)
		{
			DFS(i, visited);
			numComponents++;
			sizes[numComponents - 1] = sizeOfComponent;
			sizeOfComponent = 0;
		}
	}

	//print details for each connected component 
	for(int i = 0; i < numComponents; i++)
	{
		int outputNum = i + 1;
		cout << "For connected component " <<  outputNum << " there are " << sizes[i] << " components." << endl;
	}

	//print number of connected components
	cout << "Number of connected components: " << numComponents << endl;
}


void DFS(int vertex, bool visited[])
{
	sizeOfComponent++;
	visited[vertex] = true;
	for(list<int>::iterator it = adj[vertex].begin(); it != adj[vertex].end(); ++it)
	{
		if(visited[*it] != true)
		{
			DFS(*it, visited);
		}
	}
}


void shortestPath(int source, int dest)
{
	cout << source;

	//make adjacency matrix with all elements initialized to infinity, and then fill it with the data from the adjacency list
	//vector< vector<int> > adjMatrix(numVertex);
	int adjMatrix[numVertex][numVertex];
	
	for(int i = 0; i < numVertex; i++)
	{	
		//adjMatrix[i] = vector<int>(numVertex);
		for(int j = 0; j < numVertex; j++)
		{
			adjMatrix[i][j] = 9999;
		}
	}
	
	for(int i = 0; i < numVertex; i++)
	{

		for(list<int>::iterator it = adj[i].begin(); it != adj[i].end(); ++it)
		{
			adjMatrix[i][*it] = 1;
		}
	}

	//keeps track of the nodes discovered
	bool *discovered = new bool[numVertex];
	//set all elements in discoevered[] to false initially
	for(int i = 0; i < numVertex; i++)
	{
        discovered[i] = false;
	}
	
	 //keeps track of the shortest distance;
	int *shortestDistance = new int[numVertex];
	//vector<int> shortestDistance(numVertex);
	//set all elements in shortestDistance[] to infinity initially
	for(int i = 0; i < numVertex; ++i)
	{
		shortestDistance[i] = 9999;
	}
	//set distance to self to 0
	shortestDistance[source] = 0;


	//Dijkstra's Algorithm
	for(int i = 0; i < numVertex; ++i)
	{
		int curr = -1;
		for(int j = 0; j < numVertex; ++j)
		{	
			if(discovered[j])
			{
				continue;
			}
			if(curr == -1 || shortestDistance[j] < shortestDistance[curr])
			{
				curr = j;
			}
		}
		
		discovered[curr] = true;

		
		for(int j = 0; j < numVertex; ++j)
		{
			int route = shortestDistance[curr] + adjMatrix[curr][j];
			if(route < shortestDistance[j])
			{
				shortestDistance[j] = route;
				cout << "->" << curr;
			}
		}
		
	}

	cout << endl;

	cout << "Shortest distance: " << shortestDistance[dest] << endl;


	//clean up (deallocate memory)
	delete[] shortestDistance;
	shortestDistance = NULL;
	delete[] discovered;
	discovered = NULL;
	
}

void diameter()
{
	//Adjacency Matrix
	int  distMatrix[numVertex][numVertex];

	//initialize Matrix to inf
	for(int i = 0; i < numVertex; i++)
	{
		for(int j = 0; j < numVertex; j++)
		{
			distMatrix[i][j] = 9999;
		}
	}
	
	//set the value for each edge. 0 if u = v; 1 if edge exists.
	for(int i = 0; i < numVertex; i++)
	{
		for(list<int>::iterator it = adj[i].begin(); it != adj[i].end(); ++it)
		{
			if(i == *it)
			{
				distMatrix[i][*it] = 0;
			}
			else
			{
				distMatrix[i][*it] = 1;
			}
		}
	}
	
	//temp matrix for manipulation
	int temp[numVertex][numVertex];
	for(int i = 0; i < numVertex; i++)
	{
		for(int j = 0; j < numVertex; j++)
		{
			temp[i][j] = distMatrix[i][j];
		}
	}
	

	for (int k = 0; k < numVertex; k++)
    {
        // Pick all vertices as source one by one
        for (int i = 0; i < numVertex; i++)
        {
            // Pick all vertices as destination for the
            // above picked source
            for (int j = 0; j < numVertex; j++)
            {
                // If vertex k is on the shortest path from
                // i to j, then update the value of dist[i][j]
                if(temp[i][k] + temp[k][j] < temp[i][j])
                {
                	temp[i][j] = temp[i][k] + temp[k][j];
                }
            }
        }
    }
    
    //max val of shortest paths
    int diameter = 0;

    //find the max non inf val
    for(int i = 0; i < numVertex; i++)
    {
    	for(int j = 0; j < numVertex; j++)
    	{
    		if(temp[i][j] > diameter && temp[i][j] != 9999)
    		{
    			diameter = temp[i][j];
    		}
    	}
    }
    
    //print diameter
    cout << "Diameter: " << diameter << endl;

}



	
	
