#include	<stdio.h>

#define	CAT_NAME_LEN	25
#define	APP_NAME_LEN	50
#define	VERSION_LEN	10
#define	UNIT_SIZE	3

struct categories{
	char category[ CAT_NAME_LEN ]; // Name of category
	struct tree *root;  // Pointer to root of search tree for this category
};

struct tree{ // A binary search tree
	struct app_info app; // Information about the application
	struct tree *left;  // Pointer to the left subtree
	struct tree *right;  // Pointer to the right subtree
};

struct app_info{
	char category[ CAT_NAME_LEN ]; // Name of category
	char app_name[ APP_NAME_LEN ]; // Name of the application
	char version[ VERSION_LEN ]; // Version number
	float size; // Size of the application
	char units[ UNIT_SIZE ]; // GB or MB
	float price; // Price in $ of the application
};

struct hash_table_entry{
   char app_name[ APP_NAME_LEN ]; // Name of the application
   struct tree *app_node; // Pointer to node in tree containing the application information
};

// This definition of the tree is to be used for the full project deadline.
/*
struct tree{ // A 2-3 tree
    // In the case of a 2-node, only app_info and the left and right subtrees fields are used
    //    - app_info2 is empty and mid is NULL
    // In the case of a 3-node, all fields are used
 
	struct app_info app; // Information about the application
    struct app_info app2; // Information about the second application
	struct tree *left;  // Pointer to the left subtree; elements with keys < the key of app_info
	struct tree *mid; // Pointer to the middle subtree; elements with keys > key of app_info and < key app_info2
	struct tree *right;  // Pointer to the right subtree; elements with keys > key of app_info2
    struct tree *parent; // A pointer to the parent may be useful
};
*/

int main()
{
	printf( "Implement myAppStore here.\n" );
}
