#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h> //atoi, atof
#include <string.h>
#include <math.h>
#include <string>

#define	CAT_NAME_LEN	25
#define	APP_NAME_LEN	50
#define	VERSION_LEN		10
#define	UNIT_SIZE		3


using namespace std;

struct app_info
{
	char category[ CAT_NAME_LEN ]; // Name of category
	char app_name[ APP_NAME_LEN ]; // Name of the application
	char version[ VERSION_LEN ]; // Version number
	float size; // Size of the application
	char units[ UNIT_SIZE ]; // GB or MB
	float price; // Price in $ of the application
};
struct tree{ // A 2-3 tree
    // In the case of a 2-node, only app_info and the left and right subtrees fields are used
    //    - app_info2 is empty and mid is NULL
    // In the case of a 3-node, all fields are used
 
	app_info aI; // Information about the application
    app_info aI2; // Information about the second application
	tree *left;  // Pointer to the left subtree; elements with keys < the key of app_info
	tree *mid; // Pointer to the middle subtree; elements with keys > key of app_info and < key app_info2
	tree *right;  // Pointer to the right subtree; elements with keys > key of app_info2
    tree *parent; // A pointer to the parent may be useful
};
struct categories
{
	char category[ CAT_NAME_LEN ]; // Name of category
	tree *root; // Pointer to root of search tree for this category
};
struct hash_table_entry
{
   char app_name[ APP_NAME_LEN ]; // Name of the application
   tree *app_node; // Pointer to node in tree containing the application information
};

//forward function declarations
int hashFunction(char*, int);
bool TestForPrime(int);
int getTableSize(int);
app_info* makeApp_info(string, string, string, string, string, string);
tree* insertTree(app_info*, categories*, int);
int parseInput1();
categories* fillCategoriesArr(categories*);
int parseInput2();
hash_table_entry* fillHT(categories*, hash_table_entry*, int);
void takeCommands(categories*, hash_table_entry*, int);
void findAppName(string, hash_table_entry*, int);
void findCategoryName(string, categories*, int);
void findCategoryNameOutput(tree*);
void findPriceFree(categories*, int);
void findPriceFreeOutput(tree*);
void rangeCategoryLoHi(string, categories*, int, string, string);
void rangeCategoryLoHiOutput(tree*, float, float);
void rangeCategoryAppNameLoHi(string, categories*, int, string, string);
void rangeCategoryAppNameLoHiOutput(tree* t, char*, char*);
void freeMemoryAndExit(categories*, hash_table_entry*);
bool isLeaf(tree*);
tree* add(tree*, tree*);
tree* insert23Tree(app_info*, categories*, int, tree*);
int main();


//forward variable declarations
int numCategories = 0;
int numApps = 0;
int lineCounter = 1;
int appInfoCounter = 1;
int numInstructions;
int instructionCounter = 0;
int foundAppPriceLoHiCounter = 0;
int foundRangeCategoryAppNameLoHiCounter = 0;
int globalNumCategories;
int isleafcounter = 0;

//this is a helper function for 2-3 insert and returns whether or not a node is a leaf
bool isLeaf(tree* t)
{
	if(t->left == NULL && t->right == NULL && t->mid == NULL) return true;
	else return false;
}

//this is a helper function for 2-3 insert and does the actual adding of a node to a leaf node
tree* add(tree* t, tree* r)
{	
	if(r->aI2.app_name[0] = '~')
	{
		if(strcmp(r->aI.app_name, t->aI.app_name) < 0)
		{
			for(int i = 0; i < APP_NAME_LEN; i++)
			{
				r->aI2.app_name[i] = t->aI.app_name[i];
			}
			r->right = r->mid;
			r->mid = t->mid;
		}
		else
		{
			for(int i = 0; i < APP_NAME_LEN; i++)
			{
				r->aI2.app_name[i] = r->aI.app_name[i];
			}
			r->right = r->mid;
			for(int i = 0; i < APP_NAME_LEN; i++)
			{
				r->aI.app_name[i] = t->aI.app_name[i];
			}
			r->mid = t->mid;

		}
		return r;
	}
	else if(strcmp(r->aI.app_name, t->aI.app_name) >= 0)
	{
		tree* N1 = new tree;
		for(int i = 0; i < APP_NAME_LEN; i++)
		{
			N1->aI.app_name[i] = r->aI.app_name[i];
		}
		N1->aI2.app_name[0] = '~';
		N1->left = t;
		N1->mid = r;
		N1->right = NULL;
		t->left = r->left;
		r->left = r->mid;
		r->mid = r->right;
		r->right = NULL;
		for(int i = 0; i < APP_NAME_LEN; i++)
		{
			r->aI.app_name[i] = r->aI2.app_name[i];
		}
		for(int i = 0; i < APP_NAME_LEN; i++)
		{
			r->aI2.app_name[i] = ' ';
		}
		r->aI2.app_name[0] = '~';
		return N1;
	}
	else if(strcmp(r->aI2.app_name, t->aI.app_name) < 0)
	{
		tree* N2 = new tree;
		for(int i = 0; i < APP_NAME_LEN; i++)
		{
			N2->aI.app_name[i] = r->aI2.app_name[i];
		}
		N2->aI2.app_name[0] = '~';
		N2->left = t->mid;
		N2->mid = r->right;
		N2->right = NULL;
		t->mid = N2;
		t->left = r;
		for(int i = 0; i < APP_NAME_LEN; i++)
		{
			r->aI2.app_name[i] = ' ';
		}
		r->aI2.app_name[0] = '~';
		r->right = NULL;
		return t;
	}
	else
	{
		tree* N3 = new tree;
		for(int i = 0; i < APP_NAME_LEN; i++)
		{
			N3->aI.app_name[i] = r->aI2.app_name[i];
		}
		N3->aI2.app_name[0] = '~';
		N3->left = r;
		N3->mid = t;
		N3->right = NULL;
		t->left = r->right;
		r->right = NULL;
		r->aI2.app_name[0] = '~';
		return N3;
	}
}

//this is the insert function for the 2-3 tree, and uses hlper functions defined above
tree* insert23Tree(app_info* tmpAppInfo, categories ptrArrCategories[], int tmpNumCategories, tree* rt)
{
	tree* ret = new tree;
	for (int i = 0; i < tmpNumCategories; i++)
	{
 		if (strcmp(tmpAppInfo->category, ptrArrCategories[i].category) == 0)//search for the category of the given app from the input in the arrCategories array
		{
			if(ptrArrCategories[i].root == NULL)//once found, if the root is null, set it
			{	
				rt->aI = *tmpAppInfo;
				rt->left = rt->right = rt->mid = NULL;
				for(int k = 0; k < APP_NAME_LEN; k++)
				{
					rt->aI2.app_name[k] = ' ';
				}
				rt->aI2.app_name[0] = '~';
				ptrArrCategories[i].root = rt;
				return rt;
			}
			if(isLeaf(rt))
			{
				isleafcounter++;
				tree* toAdd = new tree;
				toAdd->aI = *tmpAppInfo;
				toAdd->aI2.app_name[0] = '~';
				toAdd->left = toAdd->mid = toAdd->right = NULL;
				return add(toAdd, rt);
			}
			if(strcmp(tmpAppInfo->app_name, rt->aI.app_name) < 0)
			{
				ret = insert23Tree(tmpAppInfo, ptrArrCategories, tmpNumCategories, rt->left);
				if(ret == rt->left) return rt;
				else return add(ret, rt);
			}
			else if((rt->aI2.app_name[0] = '~') || (strcmp(tmpAppInfo->app_name, rt->aI2.app_name)) < 0)
			{
				ret = insert23Tree(tmpAppInfo, ptrArrCategories, tmpNumCategories, rt->mid);
				if(ret == rt->mid) return rt;
				else return add(ret, rt);
			}
			else
			{
				ret = insert23Tree(tmpAppInfo, ptrArrCategories, tmpNumCategories, rt->right);
				if(ret == rt->right) return rt;
				else return add(ret, rt);
			}
		}
	}		
}

//this hash function returns a correct hash index for a given app name based on the designated hash function
int hashFunction(char app_name[APP_NAME_LEN], int size)
{
	int hash = 0;
	for (int i = 0; i < APP_NAME_LEN; i++)
	{
		hash = hash + app_name[i];
	}
	return hash % size;
}

//test for prime
bool TestForPrime(int val)
{
	int limit, factor = 2;
    limit = (long)( sqrtf( (float) val ) + 0.5f );
    while( (factor <= limit) && (val % factor) )
        factor++;
    return( factor > limit );
}

//return correct designated size of hash table
int getTableSize(int numApps)
{
	for (int i = 2*numApps; i++;)
	{
		if(TestForPrime(i))
		{
			return i;
		}
	}
}

//this function takes strings passed from one of the input parsing functions and returns a pointer to a correctly formed app_info struct
app_info* makeApp_info(string category, string app_name, string version, string size, string units, string price)
{
	app_info *A = new app_info;
	for (int i = 0; i < category.length(); i++){ A->category[i] = category.c_str()[i]; }
	for (int i = 0; i < app_name.length(); i++){ A->app_name[i] = app_name.c_str()[i]; }
	for (int i = 0; i < version.length(); i++){ A->version[i] = version.c_str()[i]; }
	for (int i = 0; i < units.length(); i++){ A->units[i] = units.c_str()[i]; }
	A->size = atof(size.c_str());
	A->price = atof(price.c_str());
	return A;
}

//binary search tree insert function
tree* insertTree(app_info* tmpAppInfo, categories ptrArrCategories[], int tmpNumCategories)
{
	tree* t = new tree;
	t->aI = *tmpAppInfo;
	t->left = NULL;
	t->right = NULL;
	tree* parent = NULL;

	
	//search for category
	for (int i = 0; i < tmpNumCategories; i++)
	{
		if (strcmp(tmpAppInfo->category, ptrArrCategories[i].category) == 0)//search for the category of the given app from the input in the arrCategories array
		{
			if(ptrArrCategories[i].root == NULL)//once found, if the root is null, set it
			{	
				ptrArrCategories[i].root = t;
			}
			else
			{
				tree* curr = ptrArrCategories[i].root;
				while(curr)
				{
					parent = curr;
					if (strcmp(t->aI.app_name, curr->aI.app_name) > 0)
					{
						curr = curr->right;
					}
					else
					{
						curr = curr->left;
					}
				}
				if (strcmp(t->aI.app_name, parent->aI.app_name) < 0)
				{
					parent->left = t;
					return t;
				}
				else
				{
					parent->right = t;
					return t;
				}
			}
			break;
		}
	}
}


//this function parses the input to get the number of categories of apps in the following input
int parseInput1()
{
	for(string line; getline(cin, line);)//get numCategories from first line, incremented lineCounter and break
	{
		numCategories = atoi(line.c_str());
		lineCounter++;
		globalNumCategories = numCategories;
		return numCategories;
	}
}

//this function parses input containing the categories of apps in the following input and populates the categories array accordingly
categories* fillCategoriesArr(categories arrCategories[])
{
	
	for(string line; getline(cin, line);)
	{
		if (lineCounter > 1 && lineCounter < numCategories + 2)//read through list of categories stopping at line number containing the number of apps
		{

			//populate array of category structs
			//array[lineCounter - 2] <---this gives accurate index of array the line should go in
			for (int i = 0; i < line.length(); i++)//copy the category name into the categories struct in the given index of arrCategories[]
			{
				arrCategories[lineCounter - 2].category[i] = line.c_str()[i];
			}
			lineCounter++;
			if (lineCounter == numCategories + 2)
			{
				return arrCategories;
			}
		}
	}
}

//this function parses input to find the number of apps in the input
int parseInput2()
{	
	for(string line; getline(cin, line);)
	{
		if (lineCounter == numCategories + 2)//stop at line with number of apps and store that number
		{	
			numApps = atoi(line.c_str());
			lineCounter++;
			if (lineCounter > numCategories + 2 && lineCounter < (numApps * 6 ) + (numCategories + 3))
			{
				return numApps;
			}	
		}
	}
}

//this function parses input containing app information, inserts them into the tree, and fills the hash table with pointers to the location of apps
hash_table_entry* fillHT(categories arrCategories[], hash_table_entry hte[], int size)
{
	string strCategory, strApp_name, strVersion, strSize, strUnits, strPrice;
	
	for(string line; getline(cin, line);)
	{
		if (lineCounter > numCategories + 2 && lineCounter < (numApps * 6 ) + (numCategories + 3))//proceed through bulk of lines processing app_info struct data correctly
		{
			if (appInfoCounter == 1)
			{
				strCategory = line;
				lineCounter++;
			}
			else if (appInfoCounter == 2)
			{
				strApp_name = line;
				lineCounter++;
			}
			else if (appInfoCounter == 3)
			{
				strVersion = line;
				lineCounter++;
			}
			else if (appInfoCounter == 4)
			{
				strSize = line;
				lineCounter++;
			}
			else if (appInfoCounter == 5)
			{
				strUnits = line;
				lineCounter++;
			}
			else if (appInfoCounter == 6)
			{
				strPrice = line;
				appInfoCounter = 0;

				tree* tmp = insertTree(makeApp_info(strCategory, strApp_name, strVersion, strSize, strUnits, strPrice), arrCategories, numCategories);

				//convert strApp_name to char for ht entry
				char tmpApp_name[ APP_NAME_LEN ];
				for (int k = 0; k < APP_NAME_LEN; k++)
				{
					tmpApp_name[k] = ' ';
					if(k == APP_NAME_LEN - 1)
					{
						tmpApp_name[k] = '\0';
					}
				}

				for (int i = 0; i < strApp_name.length(); i++)
				{
					tmpApp_name[i] = strApp_name.c_str()[i];
				}

				//hash table index
				int hteIndex = hashFunction(tmpApp_name, size);
				//this marker will allow us to know if the hash table is full and exit gracefully, rather than endlessly looping through the hash table
				int fullMarker = hteIndex;

				while (hte[hteIndex].app_node != NULL)
				{
					if(hteIndex == size - 1)
					{
						hteIndex = 0;
					}
					else if(hteIndex == fullMarker - 1)//if the index loops all the way around to the position before its starting point and that position isn't null
					{
						cout << "Hash table full, myAppstore exiting." << endl;
						freeMemoryAndExit(arrCategories, hte);
					}
					else
					{
						hteIndex = hteIndex + 1;
					}
				}

				hte[hteIndex].app_node = tmp;

				for (int i = 0; i < APP_NAME_LEN - 1; i++)
				{
					hte[hteIndex].app_name[i] = tmpApp_name[i];
				}

				strCategory.clear(); strApp_name.clear(); strVersion.clear(); strSize.clear(); strUnits.clear(); strPrice.clear();
				lineCounter++;

				if (lineCounter == (numApps * 6 ) + (numCategories + 3))
				{
					return hte; 
				}
			}
			appInfoCounter++;
		}
	}
}

//this function parses input related to commands and delegates function calls accordingly
void takeCommands(categories arrCategories[], hash_table_entry hte[], int HTSize)
{
	for(string line; getline(cin, line);)
	{
		if (lineCounter == (numApps * 6 ) + (numCategories + 3))//stop at number of instructions (near end of file) and store that number
		{
			
			numInstructions = atoi(line.c_str());
			lineCounter++;
		}
		else if (lineCounter > (numApps * 6 ) + (numCategories + 3))//process instructions (the rest of the file)
		{
			//process instructions (the rest of the file)
			if(line.substr(0,8) == "find app")
			{
				string appToFind = line.substr(10, line.length() - 11);

				findAppName(appToFind, hte, HTSize);

				lineCounter++;
				instructionCounter++;
			}
			else if(line.substr(0,13) == "find category")
			{
				string catToFind = line.substr(14, line.length() - 14);
				
				findCategoryName(catToFind, arrCategories, numCategories);
				
				lineCounter++;
				instructionCounter++;
			}
			else if(line.substr(0,15) == "find price free")
			{
				findPriceFree(arrCategories, numCategories);

				lineCounter++;
				instructionCounter++;
			}
			else if(line.substr(0, 5) == "range")
			{
				//this one requires more parsing to determine correct outcome (i.e range Games app A F, range Games price 0.00 5.00)
				string delimeter = "\" ";
				string delimeter2 = " ";
				string delimeter3 = " \'";
				string delimeter4 = "\' ";
				string rightChunk = line.substr(6, line.length() - 6);
				string catToRange = rightChunk.substr(0, rightChunk.find(delimeter) + 2);//+2 because it needs to follow through the delimeter, not end before it
				string kindOfRange;
				
				if(rightChunk.substr(rightChunk.find(delimeter) + 2, 3) == "app")
				{
					string compositeAppRange =  rightChunk.substr(rightChunk.find("app") + 5, rightChunk.length() - (rightChunk.find("app") + 6));
					string rangeAppBegin = compositeAppRange.substr(0, compositeAppRange.find(delimeter4));
					string rangeAppEnd = compositeAppRange.substr(compositeAppRange.find(delimeter3) + 2, compositeAppRange.length() - (compositeAppRange.find(delimeter2) + 2));
					string appNameCategory = rightChunk.substr(1, (rightChunk.find(delimeter) - 1));

					rangeCategoryAppNameLoHi(appNameCategory, arrCategories, numCategories, rangeAppBegin, rangeAppEnd);
					
					lineCounter++;
					instructionCounter++;
				}

				else if(rightChunk.substr(rightChunk.find(delimeter) + 2, 5) == "price")
				{
					string compositePriceRange =  rightChunk.substr(rightChunk.find("price") + 6, rightChunk.length() - rightChunk.find("price") + 6);
					string rangePriceBegin = compositePriceRange.substr(0, compositePriceRange.find(delimeter2));
					string rangePriceEnd = compositePriceRange.substr(compositePriceRange.find(delimeter2) + 1, compositePriceRange.length() - compositePriceRange.find(delimeter2) + 1);
					string appPriceCategory = rightChunk.substr(1, (rightChunk.find(delimeter) - 1));

					rangeCategoryLoHi(appPriceCategory, arrCategories, numCategories, rangePriceBegin, rangePriceEnd);

					lineCounter++;
					instructionCounter++;
				}
			}	
			else if(line.substr(0,6) == "delete")
			{
				lineCounter++;
				instructionCounter++;
				cout << "Optional delete feature not implemented." << endl;
				cout << endl;
			}
			else if( instructionCounter == 17)
			{
				//STOP TAKING INPUT
				break;
			}
		}
	}
}

//function for finding information on a given app
void findAppName(string appName, hash_table_entry hashTable[], int size)
{
	int foundSwitch = 0;
	char c[ APP_NAME_LEN + 1];
	for(int i = 0; i < APP_NAME_LEN + 1; i++)
	{
		c[i] = ' ';
		if(i == APP_NAME_LEN)
		{
			c[i] = '\0';
		}
	}
	for(int j = 0; j < appName.length(); j++)
	{
		c[j] = appName.c_str()[j];
	}
	for(int k = 0; k < size; k++)
	{
		
		if(strcmp(c, hashTable[k].app_name) == 0)
		{
			foundSwitch = 1;
			cout << "Category: " << hashTable[k].app_node->aI.category << endl;
			cout << "App Name: " << hashTable[k].app_node->aI.app_name << endl;
			cout << "Version: " << hashTable[k].app_node->aI.version << endl;
			cout << "Size: " << hashTable[k].app_node->aI.size << endl;
			cout << "Units: " << hashTable[k].app_node->aI.units << endl;
			cout << "Price: " << hashTable[k].app_node->aI.price << endl;
			cout << endl;
		}
	}
	if(foundSwitch == 0)
	{
		cout << "Application not found." << endl;
		cout << endl;
	}
}

//function for finding all apps for a given category
void findCategoryName(string catName, categories arrCategories[], int size)
{
	int foundSwitch = 0;
	char c[APP_NAME_LEN];
	for(int i = 0; i < APP_NAME_LEN; i++)
	{
		c[i] = catName.c_str()[i];
	}
	for(int j = 0; j < size; j++)
	{
		if(strcmp(c, arrCategories[j].category) == 0)
		{
			foundSwitch = 1;
			findCategoryNameOutput(arrCategories[j].root);
			cout << endl;
			
		}
	}
	if(foundSwitch == 0)
	{
		cout << "Category not found." << endl;
		cout << endl;
	}
}

//output function for finding all apps for a given category, in alphabetical order
void findCategoryNameOutput(tree* t)
{
	if(t != 0)
	{
		findCategoryNameOutput(t->left);
		cout << t->aI.app_name << endl;
		findCategoryNameOutput(t->right);
	}
}

//function for finding all free apps accross all categories
void findPriceFree(categories arrCategories[], int size)
{
	for(int i = 0; i < size; i++)
	{	
		cout << "Free apps in category \"" << arrCategories[i].category << "\":" << endl;
		findPriceFreeOutput(arrCategories[i].root);
		cout << endl;
	}
}

//output function for finding all free apps accross all categories
void findPriceFreeOutput(tree* t)
{
	if(t != 0)
	{
		findPriceFreeOutput(t->left);
		if(t->aI.price == 0)
		{
			cout << t->aI.app_name << endl;
		}
		findPriceFreeOutput(t->right);
	}
}

//function for finding a range of apps for a given category between two price points
void rangeCategoryLoHi(string catName, categories arrCategories[], int size, string strLo, string strHi)
{
	int foundSwitch = 0;
	char c[ CAT_NAME_LEN ];
	float lo = atof(strLo.c_str());
	float hi = atof(strHi.c_str());
	for(int i = 0; i < catName.length(); i++)
	{
		c[i] = catName.c_str()[i];
	}
	for(int j = 0; j < size; j++)
	{
		if(strcmp(c, arrCategories[j].category) == 0)
		{
			foundSwitch = 1;
			rangeCategoryLoHiOutput(arrCategories[j].root, lo, hi);
			cout << endl;
		}

	}
	if(foundSwitch == 0 || foundAppPriceLoHiCounter == 0)
	{
		cout << "No applications found for given range." << endl;
		cout << endl;
	}
	foundAppPriceLoHiCounter = 0;

}

//output function for finding a range of apps for a given category between two price points
void rangeCategoryLoHiOutput(tree* t, float lo, float hi)
{
	if(t != 0)
	{
		rangeCategoryLoHiOutput(t->left, lo, hi);
		if(t->aI.price >= lo && t->aI.price <= hi)
		{
			cout << t->aI.app_name << endl;
			foundAppPriceLoHiCounter++;
		}
		rangeCategoryLoHiOutput(t->right, lo, hi);
	}
}

//function for finding a range of apps for a given category between two alphabetical positions
void rangeCategoryAppNameLoHi(string catName, categories arrCategories[], int size, string strLo, string strHi)
{


	int foundSwitch = 0;
	char c[ CAT_NAME_LEN ];
	char cLo[ APP_NAME_LEN];
	char cHi[ APP_NAME_LEN];
	for(int i = 0; i <= strLo.length(); i++)
	{
		if(i == strLo.length())
			{
				cLo[i] = '\0';
				break;
			}
		cLo[i] = strLo.c_str()[i];
	}
	for(int i = 0; i <= strHi.length(); i++)
	{
		if(i == strHi.length())
			{
				cHi[i] = '\0';
				break;
			}
		cHi[i] = strHi.c_str()[i];
	}
	for(int i = 0; i <= catName.length(); i++)
	{
		if(i == catName.length())
			{
				c[i] = '\0';
				break;
			}
		c[i] = catName.c_str()[i];
	}

	for(int j = 0; j < size; j++)
	{
		if(strcmp(c, arrCategories[j].category) == 0)
		{
			foundSwitch = 1;
			rangeCategoryAppNameLoHiOutput(arrCategories[j].root, cLo, cHi);
			cout << endl;
		}

	}
	if(foundSwitch == 0 || foundRangeCategoryAppNameLoHiCounter == 0)
	{
		cout << "No applications found for given range." << endl;
		cout << endl;
	}
	foundRangeCategoryAppNameLoHiCounter = 0;

}

//output function for finding a range of apps for a given category between two alphabetical positions
void rangeCategoryAppNameLoHiOutput(tree* t, char* cLo, char* cHi)
{
	
	if(t != 0)
	{
		rangeCategoryAppNameLoHiOutput(t->left, cLo, cHi);
		if(strcmp(cLo, t->aI.app_name) < 0 && strcmp(cHi, t->aI.app_name) > 0)
		{
			cout << t->aI.app_name << endl;
			foundRangeCategoryAppNameLoHiCounter++;
		}
		rangeCategoryAppNameLoHiOutput(t->right, cLo, cHi);
	}
}

//this function terminates the process prematurely if the hash table is full
void freeMemoryAndExit(categories* arrCategories, hash_table_entry* theHashTable)
{
	//free memory allocated to hashTable and array
	delete theHashTable;
	delete arrCategories;
	theHashTable = NULL;
	arrCategories = NULL;
	exit(1);
}

//main
int main()
{
	//create categories array and fill it
	categories *arrCategories = new categories[parseInput1()];
	arrCategories = fillCategoriesArr(arrCategories);

	//create hash table of correct size
	int HTSize = getTableSize(parseInput2());
	hash_table_entry* theHashTable = new hash_table_entry[HTSize];


	//initialize structs in the hash table
	for (int i = 0; i < HTSize; i++)
	{
		theHashTable[i].app_node = NULL;
		for (int j = 0; j < APP_NAME_LEN; j++)
		{
			theHashTable[i].app_name[j] = ' ';
		}
	}
	
	//fill the hash table
	theHashTable = fillHT(arrCategories, theHashTable, HTSize);

	//take app information from the hash table and create a 2-3 tree, new hash table, and new array of categories using the pointers from the hash table
	//these new objects do not need to be included in the exit function above because if that function is called, it will have been before
	//these objects are created
	categories* arrCategoriest = new categories[globalNumCategories];
	hash_table_entry* theHashTablet = new hash_table_entry[HTSize];
	for(int i = 0; i < globalNumCategories; i++)
	{
		arrCategoriest[i] = arrCategories[i];
		arrCategoriest[i].root = NULL;

	}
	for(int i = 0; i < HTSize; i++)
	{
		if(theHashTable[i].app_node != 0)
		{
			for(int j = 0; j < globalNumCategories; j++)
			{
				if(strcmp(arrCategoriest[j].category, theHashTable[i].app_node->aI.category) == 0)
				{
					theHashTablet[i] = theHashTable[i];
					//theHashTablet[i].app_node = insert23tree(theHashTable[i].app_node->aI, arrCategoriest, globalNumCategories, theHashTable[i].app_node);
				}
			}
		}
	}


	//take commands from input
	takeCommands(arrCategories, theHashTable, HTSize);
	
	//free memory allocated to hashTable and array
	delete theHashTable;
	delete arrCategories;
	delete theHashTablet;
	delete arrCategoriest;
	theHashTable = NULL;
	arrCategories = NULL;
	theHashTablet = NULL;
	arrCategoriest = NULL;

	return 0;
}